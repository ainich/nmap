#!/usr/bin/python
# -*- coding: utf-8 -*-

# Check internal services with NMAP 

from elasticsearch import Elasticsearch
import json
import requests
import time
from datetime import datetime
from netaddr import *
from libnmap.process import NmapProcess
from libnmap.parser import NmapParser, NmapParserException
import MySQLdb



today = "logstash-"+datetime.strftime(datetime.now(), '%Y.%m.%d') #generate index name
es = Elasticsearch([{"host": "127.0.0.1", "port": 9200}]) #elastic connect
epoch = int(round(time.time() * 1000))


def do_scan(targets, options):
    parsed = None
    nmproc = NmapProcess(targets, options)
    rc = nmproc.run()
    if rc != 0:
        print("nmap scan failed: {0}".format(nmproc.stderr))
    print(type(nmproc.stdout))
    try:
        parsed = NmapParser.parse(nmproc.stdout)
    except NmapParserException as e:
        print("Exception raised while parsing scan: {0}".format(e.msg))
    return parsed

# put scan results from a nmap report to mysql
def print_scan(nmap_report, descr):
    for host in nmap_report.hosts:
        if len(host.hostnames):
            tmp_host = host.hostnames.pop()
        else:
            tmp_host = host.address

        for serv in host.services:
            pserv = "{0:>5s}/{1:3s}  {2:12s}  {3}".format(
                    str(serv.port),
                    serv.protocol,
                    serv.state,
                    serv.service)
            if len(serv.banner):
                pserv += " ({0})".format(serv.banner)
                print (tmp_host, host.address, str(serv.port), serv.protocol, serv.state, serv.service, serv.banner)
            else:
                print (tmp_host, host.address, str(serv.port), serv.protocol, serv.state, serv.service)
            es.index(index=today, doc_type="nmap", body={
            "@timestamp": epoch,
            "tmp_host": tmp_host,
            "host_address": host.address,
            "serv_port": str(serv.port),
            "serv_protocol": serv.protocol,
            "serv_state": serv.state,
            "serv_service": serv.service,
            "serv_servicefp": serv.servicefp,
            "serv_banner": serv.banner,
            "subnet_descr": descr,
            "serv_vision": "inside"
            })
    print(nmap_report.summary)

# connect to Mysql
db = MySQLdb.connect(charset="utf8", use_unicode=True, host="localhost", user="USER", passwd="PASSWORD", db="DATABASE")
# create cursor
cursor = db.cursor()
# query to Mysql
sql = """SELECT ip, org FROM net"""
cursor.execute(sql)
data =  cursor.fetchall()
for rec in data:
    subnet, descr = rec
    subnet = str(subnet)
    report = do_scan(subnet, "-T3 --open -A -sV")
    if report:
        print_scan(report, descr)
    else:
        print("No results returned")





